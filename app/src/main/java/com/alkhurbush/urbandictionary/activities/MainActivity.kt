package com.alkhurbush.urbandictionary.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.alkhurbush.urbandictionary.R
import com.alkhurbush.urbandictionary.helpers.MainAdapter
import com.alkhurbush.urbandictionary.models.Definitions
import com.alkhurbush.urbandictionary.services.DictionaryService
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.SearchView
import com.google.gson.Gson
import android.support.v7.widget.DividerItemDecoration
import com.alkhurbush.urbandictionary.models.Definition
import android.content.Context
import android.content.Intent
import android.support.v4.view.MenuItemCompat.getActionView
import android.view.View
import android.view.inputmethod.InputMethodManager


fun sortList(def : List<Definition>, isAscending : Boolean, isThumbsUp : Boolean): List<Definition> {

    if (isAscending) {
        if(isThumbsUp) {
            return def.sortedBy { it.thumbs_up }
        } else {
            return def.sortedBy { it.thumbs_down }
        }
    } else {
        if(isThumbsUp) {
            return def.sortedByDescending { it.thumbs_up }
        } else {
            return def.sortedByDescending { it.thumbs_down }
        }
    }
}

class MainActivity : AppCompatActivity(), MainAdapter.Listener {
    override fun onItemClick(definition: Definition) {
        val shareData = definition.word + "\n" + definition.definition + "\n" + definition.example
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareData)
        startActivity(Intent.createChooser(shareIntent, "Complete your action using"))
    }

    private val layoutManager : RecyclerView.LayoutManager = LinearLayoutManager(this)
    private var myMainAdapter: MainAdapter? = null
    private var myCompositeDisposable: CompositeDisposable? = null
    private var myDefinitionsList: Definitions? = null
    private val BASE_URL = "https://mashape-community-urban-dictionary.p.rapidapi.com/"
    private var requestInterface : DictionaryService? = null
    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        myCompositeDisposable = CompositeDisposable()
        initRecyclerView()
        loadData()

        val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)
        val firstStart = prefs.getBoolean("firstStart", true)

        if (firstStart) {
            showTutorialDialog()
        }

//        layoutManager.onRestoreInstanceState()
        savedInstanceState?.getString("definitions")

    }

    private fun showTutorialDialog() {
        AlertDialog.Builder(this).setTitle("Welcome").setTitle("Search for a word")

        val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)
        prefs.edit().putBoolean("firstStart", false).apply()

    }


    //Initializing the RecyclerView
    private fun initRecyclerView() {
        //Using layout manager to position the items to look like a standard ListView

        recyclerView_main.addItemDecoration(DividerItemDecoration(recyclerView_main.context, DividerItemDecoration.VERTICAL))
        layoutManager.onSaveInstanceState()
        recyclerView_main.layoutManager = layoutManager
    }

    private fun loadData() {

        //Retrofit request
        requestInterface = Retrofit.Builder().baseUrl(BASE_URL)

        //converter factory for serialization and deserialization
            .addConverterFactory(GsonConverterFactory.create())
                //support RxJava return types and build Retrofit instance
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build().create(DictionaryService::class.java)

    }

    private fun saveDefinitionsInPrefs( word:String, definitions : Definitions) {
        val definitionString = gson.toJson(definitions)
        val sharedPreferences = getSharedPreferences("Dictionary", Context.MODE_PRIVATE)
        sharedPreferences.edit().putString(word, definitionString).apply()
    }

    private fun handleResponse(definitions : Definitions) {
        println(definitions)

        val definitionsList = definitions.list
        try {
            sortList(definitionsList, isAscending = false, isThumbsUp = true)
        }catch (e: Exception) {
            throw e
        }

        myMainAdapter = MainAdapter(definitionsList, this)

        //Setting the adapter//

        recyclerView_main.adapter = myMainAdapter
        recyclerView_main.adapter?.notifyDataSetChanged()

        hideKeyboard()
    }

    private fun Activity.hideKeyboard() {
        hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        if(id == R.id.action_sort) {
            showSortDialog()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showSortDialog() {
        val options = arrayOf("Thumbs Up ⬆(Asc)", "Thumbs Up ⬇(Desc)", "Thumbs Down ⬆(Asc)", "Thumbs Down ⬇(Desc)")
        try {
            AlertDialog.Builder(this)
                .setTitle("Sort by")
                .setIcon(R.drawable.ic_action_sort)
                .setItems(options) { _, which ->
                    val selected = options[which]
                    print(selected)
                    when (which) {
                        0 -> myMainAdapter?.definitions = sortList(myMainAdapter!!.definitions,
                            isAscending = true,
                            isThumbsUp = true
                        )
                        1 -> myMainAdapter?.definitions = sortList(myMainAdapter!!.definitions,
                            isAscending = false,
                            isThumbsUp = true
                        )
                        2 -> myMainAdapter?.definitions = sortList(myMainAdapter!!.definitions,
                            isAscending = true,
                            isThumbsUp = false
                        )
                        3 -> myMainAdapter?.definitions = sortList(myMainAdapter!!.definitions,
                            isAscending = false,
                            isThumbsUp = false
                        )
                    }
                    myMainAdapter?.notifyDataSetChanged()
                }
                .create().show()
        }catch (e: Exception) {
            throw e
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        //Handel if the search is null E/SpannableStringBuilder: SPAN_EXCLUSIVE_EXCLUSIVE spans cannot have a zero length
        //    SPAN_EXCLUSIVE_EXCLUSIVE spans cannot have a zero length
         menuInflater.inflate(R.menu.toolbar_menu, menu)
        val item = menu?.findItem(R.id.action_search)

        val searchView = getActionView(item) as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            @SuppressLint("CheckResult")
            override fun onQueryTextSubmit(query: String): Boolean {

                progressBar.visibility = View.VISIBLE

                var definitions : Definitions? = null
                val sharedPreferences = getSharedPreferences("Dictionary", Context.MODE_PRIVATE)
                val definitionString = sharedPreferences.getString(query, null)
                if(definitionString != null) {
                    progressBar.visibility = View.INVISIBLE
                    println("Serving definitions from cache")
                    definitions = gson.fromJson(definitionString, Definitions::class.java)
                    handleResponse(definitions)

                    return true
                }

                requestInterface!!.getDefinitions(query)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({definitions ->  run {
                        println("Serving definitions from network")
                        progressBar.visibility = View.INVISIBLE
                        saveDefinitionsInPrefs(query, definitions)
                        handleResponse(definitions)
                    }}, { throwable -> run {
                        Log.e("An error has accrued", throwable.message)
                        // hide progressbar
//                        myProgressBar.hide()
                        progressBar.visibility = View.INVISIBLE
                    }})
                return true
            }

        })


        return true
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putString("definitions", myDefinitionsList.toString())
    }


    override fun onDestroy() {
        super.onDestroy()

        //Clear all your disposables//

        myCompositeDisposable?.clear()

    }

}
