package com.alkhurbush.urbandictionary.models

import com.google.gson.annotations.SerializedName



data class Definitions (

	@SerializedName("list") val list : List<Definition>
)