package com.alkhurbush.urbandictionary.helpers

import android.support.v7.widget.RecyclerView
import com.alkhurbush.urbandictionary.models.Definition
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.alkhurbush.urbandictionary.R
import com.alkhurbush.urbandictionary.models.Definitions
import kotlinx.android.synthetic.main.definition_row.view.*

class MainAdapter( var definitions: List<Definition>, private val listener : Listener) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    interface Listener {
        fun onItemClick(definition: Definition)
    }
    //Defining array of colours//

    private val colors : Array<String> = arrayOf("#7E57C2", "#42A5F5", "#26C6DA", "#66BB6A", "#FFEE58", "#FF7043" , "#EC407A" , "#d32f2f")
//    private val colors : Array<String> = arrayOf("#303030", "#3C3C3D", "#303030", "#3C3C3D", "#303030", "#3C3C3D", "#3C3C3D", "#3C3C3D")

    //Bind the ViewHolder

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    //Pass the position where each item should be displayed//

        holder.bind(definitions[position], colors, position, listener)

    }

    //Check how many items you have to display//

    override fun getItemCount(): Int = definitions.count()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.definition_row, parent, false)

        return ViewHolder(view)
    }


    //Creating a ViewHolder class for our RecyclerView items//

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    //Assign values from the data model, to their corresponding Views//

        fun bind(definition: Definition, colors : Array<String>, position: Int, listener : Listener) {


            var writtenOn = definition.written_on
            writtenOn = writtenOn.split("T")[0]

//            itemView.setOnClickListener{ listener.onItemClick(Definitions(listOf(definition))) }
            //itemView.setBackgroundColor(Color.parseColor(colors[position % 8]))
            itemView.setBackgroundColor(Color.WHITE)
            itemView.textView_word.text = definition.word.toUpperCase()
            itemView.textViewDefinition.text = definition.definition
            itemView.textViewExample.text = definition.example
            itemView.textViewDate.text = "by ${definition.author} at $writtenOn"
            itemView.textViewThumbUp.text = definition.thumbs_up.toString()
            itemView.textViewThumpDown.text = definition.thumbs_down.toString()

            val shareButton = itemView.imageViewShare
            shareButton.setOnClickListener { listener.onItemClick(definition) }

        }

    }
}

//searching for word doesn't exist
//share