package com.alkhurbush.urbandictionary.services

import com.alkhurbush.urbandictionary.models.Definition
import com.alkhurbush.urbandictionary.models.Definitions
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface DictionaryService {

    @Headers("X-RapidAPI-Host: mashape-community-urban-dictionary.p.rapidapi.com",
        "X-RapidAPI-Key: <Type your API key here>")
    @GET("define")
   fun getDefinitions(@Query("term") term : String) : Observable<Definitions>
}