package com.alkhurbush.urbandictionary

import android.app.Activity
import android.app.Instrumentation.ActivityResult
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4


import org.junit.Test
import org.junit.runner.RunWith

import com.alkhurbush.urbandictionary.activities.WelcomActivity
import org.junit.Rule



/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @get:Rule
    var activityRule: ActivityTestRule<WelcomActivity>
            = ActivityTestRule(WelcomActivity::class.java)

    @Test
    fun checkContinue() {
        onView(withId(R.id.buttonContinue)).perform(click())
        onView(withId(R.id.action_search)).perform(click())
    }

}
