package com.alkhurbush.urbandictionary

import com.alkhurbush.urbandictionary.activities.sortList
import com.alkhurbush.urbandictionary.models.Definitions
import com.google.gson.Gson
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import java.io.File

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class SortTests {

    var definitions : Definitions? = null

    @Before
    fun loadSampleData() {
        val definitionString = File("sampledata/testResponse.json").readText()
        val gson = Gson()
        definitions = gson.fromJson(definitionString, Definitions::class.java)
    }

    @Test
    fun thumbsUpAsc() {
        val sortedList = sortList(definitions!!.list, true, true)
        assertEquals(23, sortedList[0].thumbs_up)
    }

    @Test
    fun thumbsUpDesc() {
        val sortedList = sortList(definitions!!.list, false, true)
        assertEquals(3582, sortedList[0].thumbs_up)
    }

    @Test
    fun thumbsDownAsc() {
        val sortedList = sortList(definitions!!.list, true, false)
        assertEquals(6, sortedList[0].thumbs_down)
    }

    @Test
    fun thumbsDownDesc() {
        val sortedList = sortList(definitions!!.list, false, false)
        assertEquals(1713, sortedList[0].thumbs_down)
    }
}
